from django.shortcuts import render, redirect
from .models import MyStatus
from .forms import StatusForm
from django.http import HttpResponse

def index(request):
    status_list = MyStatus.objects.all()
    return render(request, 'status/index.html', {'mystatus': status_list})

def add(request):
    if request.method == 'POST':
        form = StatusForm(request.POST)
        if form.is_valid():
            message = request.POST['message']

            NewStatus = MyStatus(message = message)
            NewStatus.save()
    return redirect('/')

def remove(request):
    if request.method == 'POST':
        status_id = request.POST['id']
        status = MyStatus.objects.get(id = int(status_id))
        status.delete()
    return redirect('/')
