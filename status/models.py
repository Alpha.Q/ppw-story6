from django.db import models

# Create your models here.
class MyStatus(models.Model):
    message = models.CharField(max_length=255, default='')
    date = models.DateTimeField(auto_now=True)
