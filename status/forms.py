from django import forms

class StatusForm(forms.Form):
    message = forms.CharField(max_length=255, required = True)
    