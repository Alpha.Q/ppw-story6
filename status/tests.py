from django.test import TestCase
from django.test import Client
from django.test import LiveServerTestCase
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.action_chains import ActionChains
from selenium import webdriver
from django.urls import resolve
from django.http import HttpRequest
from .models import *
from .forms import *
from . import views
import unittest
import time

# Create your tests here.

class FunctionalTest(LiveServerTestCase):
    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.browser = webdriver.Chrome(chrome_options=chrome_options)
        super(FunctionalTest, self).setUp()

    def tearDown(self):
        self.browser.quit()

    def test_check_title(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn("Tugas 6 PPW 2019", self.browser.title)
        self.tearDown()

    def test_css_background(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        element = self.browser.find_element_by_css_selector('body')
        cssprop = element.value_of_css_property('background-color')
        self.assertEqual(cssprop, "rgba(255, 255, 255, 1)")
        self.tearDown()

    # def test_add_status(self):
    #     self.browser.get(self.live_server_url)
    #     time.sleep(5)
    #     status = self.browser.find_element_by_id('message')
    #     submit = self.browser.find_element_by_id('submit')
    #     status.send_keys('Halo')
    #     time.sleep(3)
    #     submit.send_keys(Keys.RETURN)
    #     self.browser.get(self.live_server_url)
    #     self.assertIn('Halo', self.browser.page_source)
    #     self.tearDown()

    def test_landing_page_welcome_message(self):
        self.browser.get(self.live_server_url)
        time.sleep(5)
        self.assertIn('Selamat Datang', self.browser.page_source)
        self.tearDown()

class FormTest(TestCase):

    def test_landing_page_response(self):
        self.response = Client().get('/')
        self.assertEqual(self.response.status_code, 200)

    def test_url_name(self):
        self.found = resolve('/')
        self.assertEqual(self.found.url_name, 'index')

    def test_intro_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('<h1>Selamat Datang</h1>', self.response.content.decode())

    def test_form_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('</form>', self.response.content.decode())

    def test_template_is_exist(self):
        self.response = Client().get('/salman')
        self.assertEqual(self.response.status_code, 404)

    def test_button_is_exist(self):
        self.response = Client().get('/')
        self.assertIn('</button>', self.response.content.decode())

    def test_form_function(self):
        self.found = resolve('/')
        self.assertEqual(self.found.func, views.index)

    # def test_post_is_valid(self):
    #     response = Client().post('/add', {'message': 'Salman Keren'})
    #     response = Client().get('/')
    #     self.assertIn('Salman Keren', response.content.decode())

    def test_response_is_posted(self):
        self.response = Client().post('/add', {'message' : 'Salman Ganteng'})
        self.assertEqual(self.response.status_code, 301)

    def test_create_models(self):
        before = MyStatus.objects.all().count()
        MyStatus.objects.create(message='Kangen dia')
        after = MyStatus.objects.all().count()
        self.assertEqual(before, after - 1)
